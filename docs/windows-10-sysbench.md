# windows 10中使用sysbench压测cpu性能


```
sysbensh是一个非常通用的benchmark工具，其提供多种方面的测试：

cpu ：提供一个简单的cpu benchmark测试
fileio：文件磁盘io的benchmark测试
memory：内存访问 benchmark测试
thread：线程调度 benchmark测试
mutex：POSIX的锁 benchmark测试
OLTP：数据库 benchmark测试，支持MySQL，Pgsql
```

本次测试我们现在只用到CPU测试工具

sysbensh 只能在linux的工作，windows 10下启用 wsl 进行操作(如何启用 wsl 请自行百度)
打开终端

把 wsl中的ubunt源文件切到 阿里云(网上有教程)


### 安装软件(linux包管理的确比windows好用)
```bash
sudo apt install sysbench
```

### CPU压测命令
```bash
# 默认参数，素数上限10000，时间10秒，单线程
sysbench cpu run
```

### 其他命令
```bash
# 素数上限20万，默认10秒，8个线程
sysbench cpu --cpu-max-prime=200000 --threads=8 run
```

### 我机器输出(E5 2637 v2)
```bash
sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 8
Initializing random number generator from current time


Prime numbers limit: 200000

Initializing worker threads...

Threads started!

CPU speed:
    events per second:   104.78

General statistics:
    total time:                          10.0669s
    total number of events:              1055

Latency (ms):
         min:                                 60.46
         avg:                                 76.11
         max:                                102.71
         95th percentile:                     80.03
         sum:                              80297.77

Threads fairness:
    events (avg/stddev):           131.8750/1.17
    execution time (avg/stddev):   10.0372/0.02
```